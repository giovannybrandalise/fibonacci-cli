exports.fibonacci = function(n) {
    _validateNullUndefined(n);
    _validateNotANumber(n);
    _validateNegativeOrZero(n);
    _validateDecimal(n);
    if (n < 3) return n - 1;
    let sequence = [];
    let result = 0;
    for (let i = 0;i < n;i++) {
        if (i < 2) {
            sequence.push(i);
        } else {
            result = sequence[i -1] + sequence[i - 2];
            sequence.push(result);
        }
    }
    return result;
}

function _validateNullUndefined(n) {
    if (n == null || n == undefined) throw Error('[n] parameter cannot be null or undefined');
}

function _validateNegativeOrZero(n) {
    if (n <= 0) throw Error('[n] parameter cannot be lower than or equal zero');
}

function _validateNotANumber(n) {
    if (isNaN(n)) throw Error('[n] parameter must be a number');
}

function _validateDecimal(n) {
    var regex = /^\d+\.\d{0,2}$/;
    if (regex.test(n)) throw Error('[n] parameter must be an integer');
}