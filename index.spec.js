const { fibonacci } = require(".");

it('should return first element from fibonacci', () => {
    expect(fibonacci(1)).toBe(0);
});

it('should return second element from fibonacci', () => {
    expect(fibonacci(2)).toBe(1);
});

it('should return third element from fibonacci', () => {
    expect(fibonacci(3)).toBe(1);
});

it('should return forth element from fibonacci', () => {
    expect(fibonacci(4)).toBe(2);
});

it('should return fifth element from fibonacci', () => {
    expect(fibonacci(5)).toBe(3);
});

it('should return ninth element from fibonacci', () => {
    expect(fibonacci(9)).toBe(21);
});

it('should return fifteenth element from fibonacci', () => {
    expect(fibonacci(15)).toBe(377);
});

it('should throw error from 0 parameter', () => {
    expect(() => fibonacci(0)).toThrow('[n] parameter cannot be lower than or equal zero');
});

it('should throw error from -2 parameter', () => {
    expect(() => fibonacci(-2)).toThrow('[n] parameter cannot be lower than or equal zero');
});

it('should throw error from null parameter', () => {
    expect(() => fibonacci(null)).toThrow('[n] parameter cannot be null or undefined');
});

it('should throw error from undefined parameter', () => {
    expect(() => fibonacci()).toThrow('[n] parameter cannot be null or undefined');
});

it('should throw error from not number parameter', () => {
    expect(() => fibonacci('a')).toThrow('[n] parameter must be a number');
});

it('should throw error from decimal number', () => {
    expect(() => fibonacci(1.2)).toThrow('[n] parameter must be an integer');
});
